import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  getAllProductsForGivenRegion,
  deleteProdukt,
  getAllProductsFromGivenShop,
  postShop,
  addRegionalProductToGivenShop,
  getShopsNamesForRegion,
  putProduct,
} from "../../api/magazine";

const initialState = {
  addProductIsOpen: false,
  addProductRegionIsOpen: false,
  newShopIsOpen: false,
  selectShopModalIsOpen: false,
  selectRegionIsOpen: false,
  editRegionsIsOpen: false,
  editManagersIsOpen: false,

  selectedShop: [{ id: 1, name: "Stonka1" }],
  allProductsFromGivenRegion: [],
  allProductsFromGivenShop: [],
  shopsNamesForRegion: [],
  shopNames: [],
};

export const fetchProductsForGivenRegion = createAsyncThunk(
  "products/regionName",
  async (regionName) => {
    const { data } = await getAllProductsForGivenRegion(regionName);
    return data;
  }
);

export const fetchShopsNamesForRegion = createAsyncThunk(
  "shops/regionName",
  async (regionName) => {
    const { data } = await getShopsNamesForRegion(regionName);
    return data;
  }
);

export const fetchProductsForGivenShop = createAsyncThunk(
  "products/shopName",
  async (shopName) => {
    const { data } = await getAllProductsFromGivenShop(shopName);
    return data;
  }
);

export const removeProduct = createAsyncThunk(
  "products/delete/shopName",
  async (shopName) => {
    await deleteProdukt(shopName);
  }
);

export const createShop = createAsyncThunk("shops", async (shopData) => {
  await postShop(shopData);
});

export const createRegionalProduct = createAsyncThunk(
  "stock/shopName",
  async (shopData) => {
    await addRegionalProductToGivenShop(shopData);
  }
);

export const editProduct = createAsyncThunk(
  "stock/editShopName",
  async (shopData) => {
    await putProduct(shopData);
  }
);

const magazineSlice = createSlice({
  name: "magazine",
  initialState,
  extraReducers: {
    [fetchProductsForGivenRegion.fulfilled]: (state, action) => {
      state.allProductsFromGivenRegion = action.payload;
    },

    [fetchShopsNamesForRegion.fulfilled]: (state, action) => {
      state.shopsNamesForRegion = action.payload;
    },

    [fetchProductsForGivenShop.fulfilled]: (state, action) => {
      state.allProductsFromGivenShop = action.payload;
    },
  },

  reducers: {
    setProductIsOpen: (state, actions) => {
      state.addProductIsOpen = actions.payload;
    },
    setNewShopIsOpen: (state, actions) => {
      state.newShopIsOpen = actions.payload;
    },
    setProductRegionIsOpen: (state, actions) => {
      state.addProductRegionIsOpen = actions.payload;
    },
    setSelectShopModal: (state, actions) => {
      state.selectShopModalIsOpen = actions.payload;
    },
    setSelectRegionModal: (state, actions) => {
      state.selectRegionIsOpen = actions.payload;
    },
    setSelectShop: (state, actions) => {
      state.selectedShop = actions.payload;
    },
    setShopName: (state, actions) => {
      state.shopNames = actions.payload;
    },
    setEditRegionModal: (state, actions) => {
      state.editRegionsIsOpen = actions.payload;
    },
    setEditManagersModal: (state, actions) => {
      state.editManagersIsOpen = actions.payload;
    },
  },
});

export const results = (state) => state.magazine;
export default magazineSlice.reducer;
export const {
  setProductIsOpen,
  setProductRegionIsOpen,
  setNewShopIsOpen,
  setSelectShopModal,
  setSelectRegionModal,
  setSelectShop,
  setShopName,
  setEditRegionModal,
  setEditManagersModal,
} = magazineSlice.actions;
