import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getAllUsers, postUser, deleteUser, putUser } from "../../api/magazine";
const initialState = {
  users: [],
  currentUser: [],
  currentEditUser: [],
};

export const fetchAllUsers = createAsyncThunk("user", async () => {
  const { data } = await getAllUsers();
  return data;
});
export const createUser = createAsyncThunk("user/create", async (user) => {
  await postUser(user);
});

export const removeUser = createAsyncThunk("user/remove", async (id) => {
  await deleteUser(id);
});

export const editUser = createAsyncThunk("user", async (user) => {
  await putUser(user);
});

const usersSlice = createSlice({
  name: "users",
  initialState,
  extraReducers: {
    [fetchAllUsers.fulfilled]: (state, action) => {
      state.users = action.payload;
    },
  },
  reducers: {
    setCurrent: (state, actions) => {
      state.currentUser = actions.payload;
    },
    setCurrentEditUser: (state, actions) => {
      state.currentEditUser = actions.payload;
    },
  },
});

export const results = (state) => state.users;
export default usersSlice.reducer;
export const { setCurrent, setCurrentEditUser } = usersSlice.actions;
