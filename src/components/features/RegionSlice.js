import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getAllRegions, postRegion, deleteRegion } from "../../api/magazine";
const initialState = {
  regions: [],
  currentRegion: [],
};

export const fetchAllRegions = createAsyncThunk("regions", async () => {
  const { data } = await getAllRegions();
  return data;
});
export const createRegion = createAsyncThunk(
  "regions/create",
  async (region) => {
    await postRegion(region);
  }
);

export const removeRegion = createAsyncThunk("regions/remove", async (id) => {
  await deleteRegion(id);
});

const regionSlice = createSlice({
  name: "regions",
  initialState,
  extraReducers: {
    [fetchAllRegions.fulfilled]: (state, action) => {
      state.regions = action.payload;
    },
  },
  reducers: {
    setCurrentRegion: (state, actions) => {
      state.currentRegion = actions.payload;
    },
  },
});

export const results = (state) => state.regions;
export default regionSlice.reducer;
export const { setCurrentRegion } = regionSlice.actions;
