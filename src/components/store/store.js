import { configureStore } from "@reduxjs/toolkit";
import magazineReducer from "../features/magazineSlice";
import usersReducer from "../features/UsersSlice";
import regionReducer from "../features/RegionSlice";
const store = configureStore({
  reducer: {
    magazine: magazineReducer,
    users: usersReducer,
    regions: regionReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;
