import React, { useState } from "react";
import { Button, Table, Input, Checkbox, Image } from "semantic-ui-react";

import { useDispatch, useSelector } from "react-redux";
import {
  fetchProductsForGivenShop,
  editProduct,
} from "../../features/magazineSlice";

import "./style.css";

const MagazineElement = ({
  id,
  name,
  price,
  amount,
  discountPrice,
  isDiscounted,
  regionId,
  description,
  url,
}) => {
  const { allProductsFromGivenShop, selectedShop, shopNames } = useSelector(
    (state) => state.magazine
  );
  const [inputAmount, setInputAmount] = useState(0);
  const [inputDiscount, setInputDiscount] = useState(0);
  const { currentUser } = useSelector((state) => state.users);
  const dispatch = useDispatch();

  const handleChangeAmount = (data) => {
    setInputAmount(data.target.value);
  };

  const handleChangeDiscount = (data) => {
    setInputDiscount(data.target.value);
  };

  const handleOrder = () => {
    allProductsFromGivenShop.forEach((current) => {
      if (current.productId === id && inputAmount > 0)
        Promise.all([
          dispatch(
            editProduct({
              shopName:
                currentUser.roleId <= 2 ? shopNames?.name : selectedShop.name,
              ...current,
              amount: current.amount + parseInt(inputAmount),
            })
          ),
        ]).then(() =>
          dispatch(
            fetchProductsForGivenShop(
              currentUser.roleId <= 2 ? shopNames?.name : selectedShop.name
            )
          )
        );
    });
    setInputAmount(0);
  };

  const handleDiscount = () => {
    allProductsFromGivenShop.forEach((current) => {
      if (current.productId === id && inputDiscount !== "")
        Promise.all([
          dispatch(
            editProduct({
              shopName:
                currentUser.roleId <= 2 ? shopNames?.name : selectedShop.name,
              ...current,
              discountPrice: inputDiscount,
            })
          ),
        ]).then(() => dispatch(fetchProductsForGivenShop(selectedShop.name)));
    });
    setInputDiscount("");
  };

  const handleChangeIsDiscounted = () => {
    allProductsFromGivenShop.forEach((current) => {
      if (current.productId === id)
        Promise.all([
          dispatch(
            editProduct({
              shopName: selectedShop.name,
              ...current,
              isDiscounted: current.isDiscounted === 0 ? 1 : 0,
            })
          ),
        ]).then(() => dispatch(fetchProductsForGivenShop(selectedShop.name)));
    });
  };
  return (
    <Table.Row key={id}>
      <Table.Cell>{name} </Table.Cell>

      <Table.Cell>
        {url !== "Brak zdjęcia" ? <Image src={url} size="tiny" /> : url}
      </Table.Cell>

      <Table.Cell>
        {description?.length ? description : "Brak opisu"}
      </Table.Cell>
      {isDiscounted === 1 ? (
        <Table.Cell>
          <del>{price}zł</del>
        </Table.Cell>
      ) : (
        <Table.Cell>{price}zł</Table.Cell>
      )}
      {isDiscounted === 1 ? (
        <Table.Cell className="discountBold">
          {currentUser.roleId <= 2 ||
          (currentUser.roleId === 3 && regionId !== currentUser.regionId) ? (
            `${discountPrice}zł`
          ) : (
            <div className="elementWrapper">
              {discountPrice}zł
              <div className="elementWrapperChild">
                <Checkbox
                  checked={isDiscounted === 1}
                  onChange={handleChangeIsDiscounted}
                />
                <Input
                  className="magazineInput"
                  value={inputDiscount}
                  onChange={handleChangeDiscount}
                />
                <Button onClick={handleDiscount}>Zmień</Button>
              </div>
            </div>
          )}
        </Table.Cell>
      ) : (
        <Table.Cell>
          {currentUser.roleId <= 2 ||
          (currentUser.roleId === 3 && regionId !== currentUser.regionId) ? (
            `${discountPrice}zł`
          ) : (
            <div className="elementWrapper">
              {discountPrice}zł
              <div className="elementWrapperChild">
                <Checkbox
                  checked={isDiscounted === 1}
                  onChange={handleChangeIsDiscounted}
                />
                <Input
                  className="magazineInput"
                  value={inputDiscount}
                  onChange={handleChangeDiscount}
                />
                <Button onClick={handleDiscount}>Zmień</Button>
              </div>
            </div>
          )}
        </Table.Cell>
      )}

      <Table.Cell>
        <div className="elementWrapper">
          {amount}
          <div className="elementWrapperChild">
            <Input
              className="magazineInput"
              type="number"
              value={inputAmount}
              onChange={handleChangeAmount}
            />
            <Button onClick={handleOrder}>Zamów</Button>
          </div>
        </div>
      </Table.Cell>
    </Table.Row>
  );
};

export default MagazineElement;
