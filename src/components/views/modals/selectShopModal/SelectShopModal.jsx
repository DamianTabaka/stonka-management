import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setSelectShopModal,
  setSelectShop,
} from "../../../features/magazineSlice";
import { Button, Modal } from "semantic-ui-react";
import Select from "react-select";

import "./style.css";

function SelectShopModal() {
  const { selectShopModalIsOpen, shopsNamesForRegion } = useSelector(
    (state) => state.magazine
  );

  const [choosedShop, setChoosedShop] = useState();
  const dispatch = useDispatch();

  const handleGroupsChange = (value) => {
    setChoosedShop(value);
  };
  const handleSelectShop = () => {
    dispatch(setSelectShop(choosedShop));
    dispatch(setSelectShopModal(false));
    setChoosedShop("");
  };
  return (
    <Modal
      size="mini"
      onClose={() => setSelectShopModal(false)}
      onOpen={() => setSelectShopModal(true)}
      open={selectShopModalIsOpen}
    >
      <Modal.Header>Wybierz sklep którym chcesz zarządzać</Modal.Header>
      <Modal.Content className="content">
        <Select
          onChange={handleGroupsChange}
          value={choosedShop}
          placeholder="Wybierz sklep..."
          options={shopsNamesForRegion}
          getOptionLabel={(o) => o.name}
          getOptionValue={(o) => o.id}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button
          color="black"
          onClick={() => dispatch(setSelectShopModal(false))}
        >
          Anuluj
        </Button>
        <Button
          content="Wybierz"
          labelPosition="right"
          icon="checkmark"
          onClick={() => handleSelectShop()}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
}

export default SelectShopModal;
