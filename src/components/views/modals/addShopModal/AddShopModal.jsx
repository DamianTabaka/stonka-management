import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setNewShopIsOpen,
  createShop,
  fetchShopsNamesForRegion,
} from "../../../features/magazineSlice";
import { Button, Modal, Input } from "semantic-ui-react";

import "./style.css";

function AddShopModal() {
  const { newShopIsOpen } = useSelector((state) => state.magazine);
  const { currentUser } = useSelector((state) => state.users);
  const { currentRegion, regions } = useSelector((state) => state.regions);
  const [shopName, setShopName] = useState();
  const dispatch = useDispatch();
  const handleShopName = (data) => {
    setShopName(data.target.value);
  };
  const regionName = () => {
    if (currentUser.roleId !== 4) {
      const reg = regions.find((curr) => curr.id === currentUser.regionId);
      return reg?.name;
    } else return currentRegion.name;
  };

  const handleAddShop = () => {
    Promise.all([
      dispatch(
        createShop({
          name: shopName,
          regionName: regionName(),
        })
      ),
    ]).then(() => dispatch(fetchShopsNamesForRegion(regionName())));
    dispatch(setNewShopIsOpen(false));
    setShopName("");
  };

  return (
    <Modal
      size="mini"
      onClose={() => setNewShopIsOpen(false)}
      onOpen={() => setNewShopIsOpen(true)}
      open={newShopIsOpen}
    >
      <Modal.Header>Dodaj sklep</Modal.Header>
      <Modal.Content className="content">
        <Input
          className="prod"
          placeholder="Dodaj sklep"
          value={shopName}
          onChange={handleShopName}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button color="black" onClick={() => dispatch(setNewShopIsOpen(false))}>
          Anuluj
        </Button>
        <Button
          content="Dodaj sklep"
          labelPosition="right"
          icon="checkmark"
          onClick={() => handleAddShop()}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
}

export default AddShopModal;
