import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setEditRegionModal,
  fetchShopsNamesForRegion,
} from "../../../features/magazineSlice";
import {
  fetchAllRegions,
  createRegion,
  removeRegion,
} from "../../../features/RegionSlice";
import { Button, Modal, Input } from "semantic-ui-react";
import Select from "react-select";

import "./style.css";

function EditRegionsModal() {
  const { editRegionsIsOpen } = useSelector((state) => state.magazine);
  const { regions } = useSelector((state) => state.regions);
  const dispatch = useDispatch();

  const [choosedRegion, setChoosedRegion] = useState("");
  const [choosedOption, setChoosedOption] = useState("");
  const [regionName, setRegionName] = useState("");

  useEffect(() => {
    dispatch(fetchShopsNamesForRegion(choosedRegion.name));
  }, [dispatch, choosedRegion]);

  const handleRegionNameChange = (e) => {
    setRegionName(e.target.value);
  };

  const handleOptionsChange = (value) => {
    setChoosedOption(value);
  };
  const handleRegionChange = (value) => {
    setChoosedRegion(value);
  };

  const options = [
    { id: 1, name: "Dodaj Region" },
    { id: 2, name: "Usuń Region" },
  ];

  const handleEditRegions = () => {
    if (choosedOption.name === "Dodaj Region") {
      Promise.all([
        dispatch(
          createRegion({
            name: regionName,
          })
        ),
      ]).then(() => {
        dispatch(fetchAllRegions());
      });
    } else if (choosedOption.name === "Usuń Region") {
      Promise.all([dispatch(removeRegion(choosedRegion.name))]).then(() => {
        dispatch(fetchAllRegions());
      });
    }
    dispatch(setEditRegionModal(false));
    setChoosedRegion("");
    setChoosedOption("");
    setRegionName("");
  };

  return (
    <Modal
      size="mini"
      onClose={() => setEditRegionModal(false)}
      onOpen={() => setEditRegionModal(true)}
      open={editRegionsIsOpen}
    >
      <Modal.Header>
        {!choosedOption.name || choosedOption.name === "Dodaj Region"
          ? "Dodaj Region"
          : "Usuń Region"}
      </Modal.Header>
      <Modal.Content className={"contentRegions"}>
        <div className="editWrapper">
          <Select
            onChange={handleOptionsChange}
            value={choosedOption}
            placeholder="Wybierz czynność..."
            options={options}
            getOptionLabel={(o) => o.name}
            getOptionValue={(o) => o.id}
          />
          {choosedOption.name === "Dodaj Region" ? (
            <div className="modalEdit">
              <Input
                className="modalEditManagerSelect"
                placeholder="Nazwa regionu.."
                value={regionName}
                onChange={handleRegionNameChange}
              />
            </div>
          ) : (
            <Select
              onChange={handleRegionChange}
              value={choosedRegion}
              placeholder="Wybierz region..."
              options={regions}
              getOptionLabel={(o) => o.name}
              getOptionValue={(o) => o.id}
            />
          )}
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button
          color="black"
          onClick={() => dispatch(setEditRegionModal(false))}
        >
          Anuluj
        </Button>
        <Button
          content={
            !choosedOption.name || choosedOption.name === "Dodaj Region"
              ? "Dodaj"
              : "Usuń"
          }
          labelPosition="right"
          icon="checkmark"
          onClick={() => handleEditRegions()}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
}

export default EditRegionsModal;
