import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setProductIsOpen } from "../../../features/magazineSlice";
import { Button, Modal, Input } from "semantic-ui-react";
import Select from "react-select";

import "./style.css";

function OrderModal() {
  const { addProductIsOpen } = useSelector((state) => state.magazine);
  const [choosedProduct, setChoosedProduct] = useState();
  const [price, setPrice] = useState();
  const [amount, setAmount] = useState();
  const dispatch = useDispatch();

  const listOfProducts = [
    { id: 1, name: "Bułki" },
    { id: 2, name: "Chipsy" },
    { id: 3, name: "Papryka" },
    { id: 4, name: "Sok pomidorowy" },
    { id: 5, name: "Por" },
    { id: 6, name: "Ananas" },
    { id: 7, name: "Granat" },
    { id: 8, name: "Rzodkiewka" },
    { id: 9, name: "Paluszki" },
    { id: 10, name: "Rogalik" },
  ];
  const handleGroupsChange = (value) => {
    setChoosedProduct(value);
  };
  const handleAmount = (data) => {
    setAmount(data.target.value);
  };
  const handlePrice = (data) => {
    setPrice(data.target.value);
  };
  const handleAddProduct = () => {
    dispatch(setProductIsOpen(false));
    setChoosedProduct("");
    setAmount("");
    setPrice("");
  };
  return (
    <Modal
      size="mini"
      onClose={() => setProductIsOpen(false)}
      onOpen={() => setProductIsOpen(true)}
      open={addProductIsOpen}
    >
      <Modal.Header>Dodaj produkt do sklepu</Modal.Header>
      <Modal.Content className="content">
        <div className="orderWrapper">
          <Select
            onChange={handleGroupsChange}
            value={choosedProduct}
            placeholder="Wybierz produkt..."
            options={listOfProducts}
            getOptionLabel={(o) => o.name}
            getOptionValue={(o) => o.id}
          />
          <div className="inputWrapper">
            <Input
              className="modalAddItemSelect"
              placeholder="Cena.."
              value={price}
              onChange={handlePrice}
            />
            <Input
              className="modalAddItemSelect"
              placeholder="Ilość.."
              type="number"
              value={amount}
              onChange={handleAmount}
            />
          </div>
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button color="black" onClick={() => dispatch(setProductIsOpen(false))}>
          Anuluj
        </Button>
        <Button
          content="Dodaj"
          labelPosition="right"
          icon="checkmark"
          onClick={() => handleAddProduct()}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
}

export default OrderModal;
