import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setSelectRegionModal } from "../../../features/magazineSlice";
import {
  setCurrentRegion,
  fetchAllRegions,
} from "../../../features/RegionSlice";
import { Button, Modal } from "semantic-ui-react";
import Select from "react-select";

import "./style.css";

function SelectRegionModal() {
  const { selectRegionIsOpen } = useSelector((state) => state.magazine);

  const { regions } = useSelector((state) => state.regions);
  const [choosedRegion, setChoosedRegion] = useState();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllRegions());
  }, [dispatch]);
  const handleRegionChange = (value) => {
    setChoosedRegion(value);
  };
  const handleSelectRegion = () => {
    dispatch(setCurrentRegion(choosedRegion));
    dispatch(setSelectRegionModal(false));
  };

  return (
    <Modal
      size="mini"
      onClose={() => setSelectRegionModal(false)}
      onOpen={() => setSelectRegionModal(true)}
      open={selectRegionIsOpen}
    >
      <Modal.Header>Wybierz region którym chcesz zarządzać</Modal.Header>
      <Modal.Content className="content">
        <Select
          onChange={handleRegionChange}
          value={choosedRegion}
          placeholder="Wybierz region..."
          options={regions}
          getOptionLabel={(o) => o.name}
          getOptionValue={(o) => o.id}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button
          color="black"
          onClick={() => dispatch(setSelectRegionModal(false))}
        >
          Anuluj
        </Button>
        <Button
          content="Wybierz"
          labelPosition="right"
          icon="checkmark"
          onClick={() => handleSelectRegion()}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
}

export default SelectRegionModal;
