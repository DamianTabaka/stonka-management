import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setEditManagersModal,
  fetchShopsNamesForRegion,
} from "../../../features/magazineSlice";
import {
  fetchAllUsers,
  createUser,
  removeUser,
  editUser,
} from "../../../features/UsersSlice";
import { Button, Modal, Input } from "semantic-ui-react";
import Select from "react-select";

import "./style.css";

function EditManagersModal() {
  const { editManagersIsOpen, shopsNamesForRegion } = useSelector(
    (state) => state.magazine
  );
  const { users } = useSelector((state) => state.users);
  const { regions } = useSelector((state) => state.regions);
  const dispatch = useDispatch();
  const [selectedManager, setSelectedManager] = useState("");
  const [choosedRegion, setChoosedRegion] = useState("");
  const [choosedShop, setChoosedShop] = useState("");
  const [choosedRole, setChoosedRole] = useState("");
  const [choosedOption, setChoosedOption] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");

  const roles = [
    { roleId: 3, role: "Region Manager" },
    { roleId: 2, role: "Shop Manager" },
  ];

  useEffect(() => {
    dispatch(fetchShopsNamesForRegion(choosedRegion.name));
  }, [dispatch, choosedRegion]);

  const handleManagersChange = (value) => {
    setSelectedManager(value);
  };
  const handleRegionChange = (value) => {
    setChoosedRegion(value);
  };

  const handleShopChange = (value) => {
    setChoosedShop(value);
  };

  const handleRoleChange = (value) => {
    setChoosedRole(value);
  };

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleOptionsChange = (value) => {
    setChoosedOption(value);
  };

  const options = [
    { id: 1, name: "Edytuj managerów" },
    { id: 2, name: "Dodaj managera" },
    { id: 3, name: "Usuń managera" },
  ];

  const handleEditManager = () => {
    if (choosedOption.name === "Edytuj managerów") {
      if (choosedRole.roleId === 3) {
        Promise.all([
          dispatch(
            editUser({
              userId: selectedManager.userId,
              username: username,
              password: password,
              regionId: choosedRegion.id,
              shopId: null,
              roleId: choosedRole.roleId,
            })
          ),
        ]).then(() => {
          dispatch(fetchAllUsers());
        });
      } else {
        Promise.all([
          dispatch(
            editUser({
              userId: selectedManager.userId,
              username: username,
              password: password,
              regionId: choosedRegion.id,
              shopId: choosedShop.id,
              roleId: choosedRole.roleId,
            })
          ),
        ]).then(() => {
          dispatch(fetchAllUsers());
        });
      }
    } else if (choosedOption.name === "Dodaj managera") {
      if (choosedRole.roleId === 3) {
        Promise.all([
          dispatch(
            createUser({
              username: username,
              password: password,
              regionId: choosedRegion.id,
              shopId: null,
              roleId: choosedRole.roleId,
            })
          ),
        ]).then(() => {
          dispatch(fetchAllUsers());
        });
      } else {
        Promise.all([
          dispatch(
            createUser({
              username: username,
              password: password,
              regionId: choosedRegion.id,
              shopId: choosedShop.id,
              roleId: choosedRole.roleId,
            })
          ),
        ]).then(() => {
          dispatch(fetchAllUsers());
        });
      }
    } else if (choosedOption.name === "Usuń managera") {
      Promise.all([
        dispatch(
          removeUser({
            userId: selectedManager.userId,
          })
        ),
      ]).then(() => {
        dispatch(fetchAllUsers());
      });
    }

    dispatch(setEditManagersModal(false));
    setSelectedManager("");
    setChoosedRegion("");
    setChoosedShop("");
    setChoosedRole("");
    setPassword("");
    setUsername("");
  };

  return (
    <Modal
      size="mini"
      onClose={() => setEditManagersModal(false)}
      onOpen={() => setEditManagersModal(true)}
      open={editManagersIsOpen}
    >
      <Modal.Header>
        {!choosedOption.name || choosedOption.name === "Edytuj managerów"
          ? "Edycja Managerów"
          : choosedOption.name === "Dodaj managera"
          ? "Dodaj Managera"
          : "Usuń Managera"}
      </Modal.Header>
      <Modal.Content
        className={
          choosedOption.name !== "Usuń managera" ? "contentManager" : ""
        }
      >
        <div className="editWrapper">
          <Select
            onChange={handleOptionsChange}
            value={choosedOption}
            placeholder="Wybierz czynność..."
            options={options}
            getOptionLabel={(o) => o.name}
            getOptionValue={(o) => o.id}
          />
          {choosedOption.name !== "Usuń managera" ? (
            <>
              <Select
                isDisabled={choosedOption.name === "Dodaj managera"}
                onChange={handleManagersChange}
                value={selectedManager}
                placeholder="Wybierz managera..."
                options={users?.filter((current) => {
                  return current.roleId > 1 && current.roleId < 4;
                })}
                getOptionLabel={(o) => o.username}
                getOptionValue={(o) => o.userId}
              />

              <Select
                onChange={handleRegionChange}
                value={choosedRegion}
                placeholder="Wybierz region..."
                options={regions}
                getOptionLabel={(o) => o.name}
                getOptionValue={(o) => o.id}
              />
              <Select
                onChange={handleRoleChange}
                value={choosedRole}
                placeholder="Wybierz role..."
                options={roles}
                getOptionLabel={(o) => o.role}
                getOptionValue={(o) => o.roleId}
              />
              <Select
                isDisabled={!choosedRole || choosedRole.roleId === 3}
                onChange={handleShopChange}
                value={choosedShop}
                placeholder="Wybierz sklep..."
                options={shopsNamesForRegion}
                getOptionLabel={(o) => o.name}
                getOptionValue={(o) => o.id}
              />

              <div className="inputWrapper">
                <Input
                  className="modalAddItemSelect"
                  placeholder="Username.."
                  value={username}
                  onChange={handleUsernameChange}
                />
                <Input
                  className="modalAddItemSelect"
                  placeholder="Password.."
                  type="password"
                  value={password}
                  onChange={handlePasswordChange}
                />
              </div>
            </>
          ) : (
            <Select
              onChange={handleManagersChange}
              value={selectedManager}
              placeholder="Usuń managera..."
              options={users?.filter((current) => {
                return current.roleId > 1 && current.roleId < 4;
              })}
              getOptionLabel={(o) => o.username}
              getOptionValue={(o) => o.userId}
            />
          )}
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button
          color="black"
          onClick={() => dispatch(setEditManagersModal(false))}
        >
          Anuluj
        </Button>
        <Button
          content={
            !choosedOption.name || choosedOption.name === "Edytuj managerów"
              ? "Edytuj"
              : choosedOption.name === "Dodaj managera"
              ? "Dodaj"
              : "Usuń"
          }
          labelPosition="right"
          icon="checkmark"
          onClick={() => handleEditManager()}
          positive
        />
      </Modal.Actions>
    </Modal>
  );
}

export default EditManagersModal;
