import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setProductRegionIsOpen,
  fetchProductsForGivenShop,
  createRegionalProduct,
  fetchShopsNamesForRegion,
} from "../../../features/magazineSlice";
import { Button, Modal, Input } from "semantic-ui-react";
import Select from "react-select";

import "./style.css";

function RegionAddProductModal() {
  const {
    addProductRegionIsOpen,
    selectedShop,
    shopNames,
    shopsNamesForRegion,
  } = useSelector((state) => state.magazine);
  const { regions, currentRegion } = useSelector((state) => state.regions);
  const regionName = () => {
    if (currentUser.roleId !== 4) {
      const reg = regions.find((curr) => curr.id === currentUser.regionId);
      return reg?.name;
    } else return currentRegion?.name;
  };
  const [productName, setProductName] = useState();
  const { currentUser } = useSelector((state) => state.users);
  const [price, setPrice] = useState("");
  const [amount, setAmount] = useState("");
  const [description, setDescription] = useState("");
  const [uri, setUri] = useState("");
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [choosedRegion, setChoosedRegion] = useState("");
  const [choosedShop, setChoosedShop] = useState("");

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      fetchProductsForGivenShop(
        currentUser.roleId <= 2 ? shopNames?.name : selectedShop?.name
      )
    );
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchShopsNamesForRegion(choosedRegion.name));
  }, [dispatch, choosedRegion]);

  const handleCategoryChange = (value) => {
    setSelectedCategory(value);
  };

  const handleShopChange = (value) => {
    setChoosedShop(value);
  };

  const handleAmount = (data) => {
    setAmount(data.target.value);
  };
  const handlePrice = (data) => {
    setPrice(data.target.value);
  };

  const handleUri = (data) => {
    setUri(data.target.value);
  };

  const handleDescription = (data) => {
    setDescription(data.target.value);
  };

  const handleProductName = (data) => {
    setProductName(data.target.value);
  };

  const handleRegionChange = (value) => {
    setChoosedRegion(value);
  };

  const categories = [
    { categoriesId: 1, name: "Pieczywo" },
    { categoriesId: 2, name: "Slodycze" },
    { categoriesId: 3, name: "Nabial" },
    { categoriesId: 4, name: "Napoje" },
    { categoriesId: 5, name: "Alkohole" },
    { categoriesId: 6, name: "Mieso" },
  ];

  const handleAddProduct = () => {
    if (currentUser.roleId !== 4) {
      Promise.all([
        dispatch(
          createRegionalProduct({
            shopName: selectedShop.name || choosedShop.name,
            discountPrice: "0",
            isDiscounted: 0,
            price: price,
            productName: productName,
            regionName: regionName(),
            amount: amount,
            productDescription: description,
            imgUrl: uri ? uri : "",
            categoryName: selectedCategory.name,
          })
        ),
      ]).then(() =>
        dispatch(
          fetchProductsForGivenShop(
            currentUser.roleId <= 2 ? shopNames?.name : selectedShop?.name
          )
        )
      );
    } else {
      Promise.all([
        dispatch(
          createRegionalProduct({
            shopName: selectedShop.name || choosedShop.name,
            discountPrice: "0",
            isDiscounted: 0,
            price: price,
            productName: productName,
            regionName: choosedRegion.name,
            amount: amount,
            productDescription: description,
            imgUrl: uri ? uri : "",
            categoryName: selectedCategory.name,
          })
        ),
      ]).then(() =>
        dispatch(
          fetchProductsForGivenShop(
            currentUser.roleId <= 2 ? shopNames?.name : selectedShop?.name
          )
        )
      );
    }

    dispatch(setProductRegionIsOpen(false));
    setProductName("");
    setAmount("");
    setPrice("");
    setDescription("");
    setUri("");
    setSelectedCategory([]);
  };
  console.log(selectedCategory.length > 0);
  return (
    <Modal
      size="mini"
      onClose={() => setProductRegionIsOpen(false)}
      onOpen={() => setProductRegionIsOpen(true)}
      open={addProductRegionIsOpen}
    >
      <Modal.Header>
        {currentUser.roleId !== 4
          ? "Dodaj produkt regionalny do sklepu"
          : "Dodaj produkt do sklepu"}
      </Modal.Header>
      <Modal.Content
        className={currentUser.roleId !== 4 ? "contentAddProduct" : ""}
      >
        <div className="orderWrapperAddProduct">
          <Input
            placeholder="Nazwa produktu.."
            value={productName}
            onChange={handleProductName}
          />
          <div className="inputWrapper">
            <Input
              className="modalAddItemSelect"
              placeholder="Cena.."
              value={price}
              onChange={handlePrice}
            />
            <Input
              className="modalAddItemSelect"
              placeholder="Ilość.."
              type="number"
              value={amount}
              onChange={handleAmount}
            />
          </div>
          <Input
            placeholder="Opis produktu.."
            value={description}
            onChange={handleDescription}
          />
          <Input
            placeholder="Wrzuć link obrazka produktu.."
            value={uri}
            onChange={handleUri}
          />
          {currentUser.roleId === 4 && (
            <>
              <Select
                onChange={handleRegionChange}
                value={choosedRegion}
                placeholder="Wybierz region..."
                options={regions}
                getOptionLabel={(o) => o.name}
                getOptionValue={(o) => o.id}
              />
              <Select
                isDisabled={!choosedRegion.name}
                onChange={handleShopChange}
                value={choosedShop}
                placeholder="Wybierz sklep..."
                options={shopsNamesForRegion}
                getOptionLabel={(o) => o.name}
                getOptionValue={(o) => o.id}
              />
            </>
          )}
          <Select
            onChange={handleCategoryChange}
            value={selectedCategory}
            placeholder="Wybierz kategorię..."
            options={categories}
            getOptionLabel={(o) => o.name}
            getOptionValue={(o) => o.categoriesId}
          />
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button
          color="black"
          onClick={() => dispatch(setProductRegionIsOpen(false))}
        >
          Anuluj
        </Button>

        {currentUser.roleId === 4 ? (
          <Button
            disabled={
              !price ||
              !amount ||
              !description ||
              selectedCategory.length === 0 ||
              !choosedRegion ||
              !choosedShop
            }
            content="Dodaj"
            labelPosition="right"
            icon="checkmark"
            onClick={() => handleAddProduct()}
            positive
          />
        ) : (
          <Button
            content="Dodaj"
            labelPosition="right"
            icon="checkmark"
            onClick={() => handleAddProduct()}
            positive
          />
        )}
      </Modal.Actions>
    </Modal>
  );
}

export default RegionAddProductModal;
