import React from "react";
import { useSelector } from "react-redux";
import MagazineElement from "../magazineElement/MagazineElement";
import { Table } from "semantic-ui-react";

import "./style.css";
const MagazineTable = () => {
  const { allProductsFromGivenShop } = useSelector((state) => state.magazine);

  const renderMagazineElement = () =>
    allProductsFromGivenShop.map((current) => (
      <MagazineElement
        key={current.productId}
        url={current.imgUrl ? current.imgUrl : "Brak zdjęcia"}
        id={current.productId}
        name={current.productName}
        amount={current.amount}
        price={current.price}
        discountPrice={current.discountPrice}
        isDiscounted={current.isDiscounted}
        regionId={current.productRegionId}
        description={current.productDescription}
      />
    ));

  return (
    <div className="magazineTableWidth">
      <Table fixed celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={3}>Nazwa</Table.HeaderCell>
            <Table.HeaderCell width={2}>Zdjęcie</Table.HeaderCell>
            <Table.HeaderCell width={4}>Opis</Table.HeaderCell>
            <Table.HeaderCell width={2}>Cena</Table.HeaderCell>
            <Table.HeaderCell width={5}>Cena promocyjna</Table.HeaderCell>
            <Table.HeaderCell width={4}>Ilość</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {allProductsFromGivenShop.length > 0
            ? renderMagazineElement()
            : "Wybierz odpowieni sklep z listy"}
        </Table.Body>
      </Table>
    </div>
  );
};

export default MagazineTable;
