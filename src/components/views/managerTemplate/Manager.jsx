import React, { useState, useEffect } from "react";
import MagazineTable from "../magazineTable/MagazineTable";
import Header from "../header/Header";
import {
  fetchProductsForGivenShop,
  fetchShopsNamesForRegion,
  setShopName,
} from "../../features/magazineSlice";
import { useDispatch, useSelector } from "react-redux";
import OrderModal from "../modals/orderModal/OrderModal";
import RegionAddProductModal from "../modals/addRegionProductModal/RegionAddProductModal";
import AddShopModal from "../modals/addShopModal/AddShopModal";
import SelectShopModal from "../modals/selectShopModal/SelectShopModal";
import SelectRegionModal from "../modals/selectRegionModal/SelectRegionModal";
import EditRegionsModal from "../modals/editRegionsModal/EditRegionsModal";
import EditManagersModal from "../modals/editManagersModal/EditManagersModal";
const Manager = ({ who }) => {
  const [visible, setVisible] = useState(false);
  const { currentUser } = useSelector((state) => state.users);
  const { currentRegion, regions } = useSelector((state) => state.regions);
  const { selectedShop, shopsNamesForRegion, shopNames } = useSelector(
    (state) => state.magazine
  );

  const dispatch = useDispatch();
  // ? Wyszukaj sklep po ID regionu
  const regionName = () => {
    if (currentUser.roleId !== 4) {
      const reg = regions.find((curr) => curr.id === currentUser.regionId);
      return reg?.name;
    } else return currentRegion.name;
  };

  useEffect(() => {
    dispatch(fetchShopsNamesForRegion(regionName()));
  }, [dispatch, currentRegion.name]);

  useEffect(() => {
    dispatch(
      setShopName(
        shopsNamesForRegion.find((curr) => curr.id === currentUser.shopId)
      )
    );
  }, [dispatch, shopsNamesForRegion]);
  // ? //////////////////////////
  useEffect(() => {
    dispatch(
      fetchProductsForGivenShop(
        currentUser.roleId <= 2 ? shopNames?.name : selectedShop.name
      )
    );
  }, [dispatch, shopNames, selectedShop, currentUser]);

  return (
    <div>
      <Header setVisible={setVisible} who={who} />
      {visible && <MagazineTable />}
      {/* <OrderModal /> */}
      {currentUser.roleId >= 3 && (
        <>
          <RegionAddProductModal />
          <AddShopModal />
          <SelectShopModal />
          <SelectRegionModal />
        </>
      )}
      {currentUser.roleId === 4 && (
        <>
          <EditRegionsModal />
          <EditManagersModal />
        </>
      )}
    </div>
  );
};

export default Manager;
