import React from "react";
import { Menu } from "semantic-ui-react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  setProductRegionIsOpen,
  setNewShopIsOpen,
  setSelectShopModal,
  setEditRegionModal,
  setEditManagersModal,
} from "../../features/magazineSlice";
import { setSelectRegionModal } from "../../features/magazineSlice";
const Header = ({ setVisible, who }) => {
  const { addProductRegionIsOpen } = useSelector((state) => state.magazine);
  const { currentUser } = useSelector((state) => state.users);
  const { currentRegion, regions } = useSelector((state) => state.regions);
  const { selectedShop, shopNames } = useSelector((state) => state.magazine);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleLogout = () => {
    navigate("/");
  };

  const regionName = () => {
    if (currentUser.roleId !== 4) {
      const reg = regions.find((curr) => curr.id === currentUser.regionId);
      return reg?.name;
    } else return currentRegion.name;
  };

  const handleShowTable = () => {
    setVisible((prev) => !prev);
  };

  return (
    <Menu secondary>
      <Menu.Item name="Pokaż magazyn" onClick={handleShowTable} />

      {who === "Regional Manager" && (
        <>
          <Menu.Item
            name="Wybierz sklep"
            onClick={() => dispatch(setSelectShopModal(true))}
          />
          {!addProductRegionIsOpen && (
            <Menu.Item
              name="Dodaj produkt regionalny do sklepu"
              onClick={() => dispatch(setProductRegionIsOpen(true))}
            />
          )}
          {addProductRegionIsOpen && (
            <Menu.Item
              name="Anuluj"
              onClick={() => dispatch(setProductRegionIsOpen(false))}
            />
          )}
          <Menu.Item
            name="Utwórz Nowy sklep"
            onClick={() => dispatch(setNewShopIsOpen(true))}
          />
        </>
      )}

      {who === "Buisness Manager" && (
        <>
          <Menu.Item
            name="Wybierz Region"
            onClick={() => dispatch(setSelectRegionModal(true))}
          />
          <Menu.Item
            name="Wybierz sklep"
            onClick={() => dispatch(setSelectShopModal(true))}
          />
          <Menu.Item
            name="Edytuj regiony"
            onClick={() => dispatch(setEditRegionModal(true))}
          />
          <Menu.Item
            name="Edytuj managerów"
            onClick={() => dispatch(setEditManagersModal(true))}
          />

          {!addProductRegionIsOpen && (
            <Menu.Item
              name="Dodaj produkt do wybranego sklepu"
              onClick={() => dispatch(setProductRegionIsOpen(true))}
            />
          )}
          {addProductRegionIsOpen && (
            <Menu.Item
              name="Anuluj"
              onClick={() => dispatch(setProductRegionIsOpen(false))}
            />
          )}
          <Menu.Item
            name="Utwórz Nowy sklep"
            onClick={() => dispatch(setNewShopIsOpen(true))}
          />
        </>
      )}

      <Menu.Menu position="right">
        {currentUser.roleId === 4 ? (
          <Menu.Item
            name={`${who} ${currentRegion?.name ? currentRegion?.name : ""} ${
              selectedShop.name ? selectedShop.name : ""
            }`}
          />
        ) : currentUser.roleId <= 2 ? (
          <Menu.Item name={`${who} ${shopNames?.name}`} />
        ) : (
          <Menu.Item name={`${who} ${regionName()}`} />
        )}

        <Menu.Item name="wyloguj" onClick={handleLogout} />
      </Menu.Menu>
    </Menu>
  );
};

export default Header;
