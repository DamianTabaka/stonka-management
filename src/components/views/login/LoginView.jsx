import React, { useState, useEffect } from "react";
import { Form, Grid, Header, Segment, Button } from "semantic-ui-react";
import { useSelector, useDispatch } from "react-redux";
import { setCurrent, fetchAllUsers } from "../../features/UsersSlice";
import { fetchAllRegions } from "../../features/RegionSlice";
import { useNavigate } from "react-router-dom";
import "./index.css";

const LoginView = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [wasSended, setWasSended] = useState(false);
  const [badPassword, setBadPassword] = useState();
  const { users } = useSelector((state) => state.users);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(fetchAllUsers());
    dispatch(fetchAllRegions());
  }, [dispatch]);

  const setNav = (user) => {
    if (user.roleId === 2) {
      navigate("/shopmanager");
    } else if (user.roleId === 3) {
      navigate("/regionalmanager");
    } else if (user.roleId === 4) {
      navigate("/buisnessmanager");
    }
  };

  const handleSubmit = (e) => {
    setWasSended(true);
    e.preventDefault();

    users.map((user) => {
      if (login === user.username && password === user.password) {
        dispatch(setCurrent(user));
        setNav(user);
      } else if (login.length > 0 && password.length > 0) {
        setBadPassword("Podano błędne dane");
      }
    });
  };
  const loginEmptyError = !login &&
    wasSended && {
      content: "Wpisz login!",
      pointing: "below",
    };

  const passwordEmptyError = !password &&
    wasSended && {
      content: "Wpisz hasło!",
      pointing: "below",
    };

  return (
    <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="teal" textAlign="center">
          Stonka managment studio
        </Header>
        <Header as="h3" color="teal" textAlign="center">
          Wpisz dane by się zalogować
        </Header>
        <Form size="large">
          <Segment stacked>
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              placeholder="Login..."
              onChange={(e) => {
                setLogin(e.target.value);
              }}
              error={loginEmptyError || badPassword}
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              type="password"
              placeholder="Hasło..."
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              error={passwordEmptyError || badPassword}
            />

            <Button
              size="big"
              type="submit"
              onClick={handleSubmit}
              color="blue"
            >
              Zaloguj
            </Button>
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

export default LoginView;
