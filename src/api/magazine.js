import client from "./client";

export const getAllProductsForGivenRegion = async (regionName) => {
  return await client(`http://localhost:8081/products/${regionName}`, {
    method: "GET",
  });
};

export const getAllProductsFromGivenShop = async (shopName) => {
  return await client(`http://localhost:8081/shops/products/${shopName}`, {
    method: "GET",
  });
};

export const getShopsNamesForRegion = async (regionName) => {
  return await client(`http://localhost:8081/shops/${regionName}`, {
    method: "GET",
  });
};

export const deleteProdukt = async (shopName) => {
  return await client(`http://localhost:8081/products/delete/${shopName}`, {
    method: "DELETE",
  });
};

export const postShop = async (data) => {
  return await client(`http://localhost:8081/shops`, {
    method: "POST",
    data: data,
  });
};

export const addRegionalProductToGivenShop = async (data) => {
  return await client(`http://localhost:8081/stocks/${data.shopName}`, {
    method: "POST",
    data: data,
  });
};

export const putProduct = async (data) => {
  return await client(`http://localhost:8081/stocks/${data.shopName}`, {
    method: "PUT",
    data: data,
  });
};

export const getAllUsers = async () => {
  return await client(`http://localhost:8081/user`, {
    method: "GET",
  });
};

export const postUser = async (data) => {
  return await client(`http://localhost:8081/user`, {
    method: "POST",
    data: data,
  });
};

export const deleteUser = async (data) => {
  return await client(`http://localhost:8081/user/${data.userId}`, {
    method: "DELETE",
  });
};

export const putUser = async (data) => {
  return await client(`http://localhost:8081/user/update`, {
    method: "PUT",
    data: data,
  });
};

export const getAllRegions = async () => {
  return await client(`http://localhost:8081/regions`, {
    method: "GET",
  });
};

export const postRegion = async (data) => {
  return await client(`http://localhost:8081/regions`, {
    method: "POST",
    data: data,
  });
};

export const deleteRegion = async (regionName) => {
  return await client(`http://localhost:8081/regions/${regionName}`, {
    method: "DELETE",
  });
};
