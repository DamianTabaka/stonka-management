import "./App.css";
import Manager from "./components/views/managerTemplate/Manager";
import LoginView from "./components/views/login/LoginView";
import { Route, Routes } from "react-router-dom";

function App() {
  return (
    <div className="wrapper">
      <Routes>
        <Route path="/" element={<LoginView />} />
        <Route
          path="/buisnessmanager"
          element={<Manager who={"Buisness Manager"} />}
        />
        <Route
          path="/regionalmanager"
          element={<Manager who={"Regional Manager"} />}
        />
        <Route path="/shopmanager" element={<Manager who={"Shop Manager"} />} />
      </Routes>
    </div>
  );
}

export default App;
